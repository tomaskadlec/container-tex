container-tex
=============

.. _`Debian testing`: https://wiki.debian.org/DebianTesting
.. _`TeX Live`: https://www.tug.org/texlive/
.. _`run.sh`: run.sh
.. |run.sh| replace:: ``run.sh``   

This project builds `Debian testing`_ based images with `TeX Live`_ 
in it.

Installation
------------

#. Download or clone the project, e.g. ::

     git clone https://tomaskadlec@gitlab.com/tomaskadlec/container-tex.git /usr/local/container/tex
   
#. Add |run.sh|_ script to ``PATH``, e.g. symlink it to a directory already in
   ``PATH``::

     ln -s /usr/local/container/tex/run.sh /usr/local/bin/tex-container

Usage
-----

Just run |run.sh|_ (or whatever it is called in ``PATH``). It will start a
container change identity to current user and change to current working
directory. The container will be removed if it is stopped.

License
-------

.. _`The MIT License`: LICENSE

This project is licensed under `The MIT License`_.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
