FROM debian:stretch
RUN sed -i 's|httpredir.debian.org|ftp.cz.debian.org|g' /etc/apt/sources.list
RUN sed -i 's|testing|stretch|g' /etc/apt/sources.list

RUN apt-get update && apt-get install -y \
    apt-utils \
    locales \
    gosu \
    less \
    procps \
    wget \
    git \
    make \
    texlive-base \
    texlive-pictures \
    texlive-science \
    texlive-xetex \
    texlive-lang-czechslovak \
    texlive-lang-english \
    pdfgrep \
    python3-pip \
    python3-virtualenv

RUN echo -e "en_US.UTF-8 UTF-8\ncs_CZ.UTF-8 UTF-8\nC.UTF-8 UTF-8" > /etc/locale.gen && \
    echo -e "C.UTF-8" > /etc/default/locale && \
    dpkg-reconfigure --frontend=noninteractive locales

ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

ADD ["init.sh", "*.init.sh", "/usr/local/container_init/"]
RUN chmod +x /usr/local/container_init/init.sh
ENTRYPOINT ["/bin/bash", "/usr/local/container_init/init.sh"]
CMD ["/bin/bash"]
