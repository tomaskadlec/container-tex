#!/bin/bash
# Starts a container for current user

while getopts :h OPT; do
    case "$OPT" in
        *)
            usage
            exit 1
            ;;        
    esac
    shift $((OPTIND-1))
done

WORKDIR="${1:-$PWD}"

docker run -ti --rm \
    --env "CONTAINER_UID=$(id -u)" \
    --env "CONTAINER_GID=$(id -g)" \
    --volume="/home/$USER:/home/$USER" \
    --env="DISPLAY" \
    --workdir="$WORKDIR" \
    registry.gitlab.com/tomaskadlec/container-tex:latest
